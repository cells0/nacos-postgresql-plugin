package com.cell.nacos.plugin.postgresql.helper;

import java.util.List;

/**
 * @author 赵凯
 * @since 1.0.0
 */
public class SqlHelper {

    public static String parse(String content) {
        return content == null ? null : content.replace("'", "''");
    }

    public static String getInSql(List<?> list) {
        StringBuilder result = new StringBuilder("'");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            result.append(parse(list.get(i).toString()))
                .append("'");
            if (i != size - 1) {
                result.append(", ");
            }
        }

        return result.toString();
    }
}
