package com.cell.nacos.plugin.postgresql.helper;

import java.util.Collection;

/**
 * @author 赵凯
 * @since 2023-10-19
 */
public final class CollectionUtils {

    private CollectionUtils() {

    }

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }
}
