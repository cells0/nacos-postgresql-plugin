package com.cell.nacos.plugin.postgresql.mapper;

import com.alibaba.nacos.plugin.datasource.constants.TableConstant;
import com.alibaba.nacos.plugin.datasource.mapper.AbstractMapper;
import com.alibaba.nacos.plugin.datasource.mapper.ConfigInfoTagMapper;
import com.cell.nacos.plugin.postgresql.common.PgConstants;

/**
 * The PostgreSQL implementation of ConfigInfoTagMapper.
 * @author 赵凯
 * @since 1.0.0
 */
public class ConfigInfoTagMapperByPgSql extends AbstractMapper implements ConfigInfoTagMapper {

    @Override
    public String updateConfigInfo4TagCas() {
        return "UPDATE \"config_info_tag\" SET \"content\" = ?, \"md5\" = ?, \"src_ip\" = ?,\"src_user\" = ?,\"gmt_modified\" = ?,\"app_name\" = ? "
            + "WHERE \"data_id\" = ? AND \"group_id\" = ? AND \"tenant_id\" = ? AND \"tag_id\" = ? AND (\"md5\" = ? OR \"md5\" IS NULL OR \"md5\" = '')";
    }

    @Override
    public String findAllConfigInfoTagForDumpAllFetchRows(int startRow, int pageSize) {
        return " SELECT \"t\".\"id\",\"data_id\",\"group_id\",\"tenant_id\",\"tag_id\",\"app_name\",\"content\",\"md5\",\"gmt_modified\" "
            + " FROM (  SELECT \"id\" FROM \"config_info_tag\"  ORDER BY \"id\" LIMIT " + pageSize + " OFFSET " + startRow + " ) "
            + "\"g\", \"config_info_tag\" \"t\"  WHERE \"g\".\"id\" = \"t\".\"id\"  ";
    }

    @Override
    public String getTableName() {
        return TableConstant.CONFIG_INFO_TAG;
    }

    @Override
    public String getDataSource() {
        return PgConstants.PG_PLATFORM;
    }
}
