package com.cell.nacos.plugin.postgresql.mapper;

import com.alibaba.nacos.plugin.datasource.constants.TableConstant;
import com.alibaba.nacos.plugin.datasource.mapper.AbstractMapper;
import com.alibaba.nacos.plugin.datasource.mapper.ConfigInfoAggrMapper;
import com.cell.nacos.plugin.postgresql.common.PgConstants;
import com.cell.nacos.plugin.postgresql.helper.SqlHelper;

import java.util.List;

/**
 * The PostgreSQL implementation of ConfigInfoAggrMapper.
 * @author 赵凯
 * @since 1.0.0
 */
public class ConfigInfoAggrMapperByPgSql extends AbstractMapper implements ConfigInfoAggrMapper {

    @Override
    public String batchRemoveAggr(List<String> datumList) {
        final StringBuilder datumString = new StringBuilder();
        for (String datum : datumList) {
            datumString.append('\'').append(SqlHelper.parse(datum)).append("',");
        }
        datumString.deleteCharAt(datumString.length() - 1);
        return "DELETE FROM \"config_info_aggr\" WHERE \"data_id\" = ? AND \"group_id\" = ? AND \"tenant_id\" = ? AND \"datum_id\" IN ("
            + datumString + ")";
    }

    @Override
    public String aggrConfigInfoCount(int size, boolean isIn) {
        StringBuilder sql = new StringBuilder(
            "SELECT COUNT(1) FROM \"config_info_aggr\" WHERE \"data_id\" = ? AND \"group_id\" = ? AND \"tenant_id\" = ? AND \"datum_id\"");
        if (isIn) {
            sql.append(" IN (");
        } else {
            sql.append(" NOT IN (");
        }
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sql.append(", ");
            }
            sql.append('?');
        }
        sql.append(')');

        return sql.toString();
    }

    @Override
    public String findConfigInfoAggrIsOrdered() {
        return "SELECT \"data_id\",\"group_id\",\"tenant_id\",\"datum_id\",\"app_name\",\"content\" FROM "
            + "\"config_info_aggr\" WHERE \"data_id\" = ? AND \"group_id\" = ? AND \"tenant_id\" = ? ORDER BY \"datum_id\"";
    }

    @Override
    public String findConfigInfoAggrByPageFetchRows(int startRow, int pageSize) {
        return "SELECT \"data_id\",\"group_id\",\"tenant_id\",\"datum_id\",\"app_name\",\"content\" FROM \"config_info_aggr\" WHERE \"data_id\"= ? AND "
            + "\"group_id\"= ? AND \"tenant_id\"= ? ORDER BY \"datum_id\" LIMIT " + pageSize + " OFFSET " + startRow;
    }

    @Override
    public String findAllAggrGroupByDistinct() {
        return "SELECT DISTINCT \"data_id\", \"group_id\", \"tenant_id\" FROM \"config_info_aggr\"";
    }

    @Override
    public String getTableName() {
        return TableConstant.CONFIG_INFO_AGGR;
    }

    @Override
    public String getDataSource() {
        return PgConstants.PG_PLATFORM;
    }
}
