package com.cell.nacos.plugin.postgresql.helper;

/**
 * @author 赵凯
 * @since 2023-10-19
 */
public final class StringUtils {

    public static final String EMPTY = "";

    private StringUtils() {

    }

    public static boolean isBlank(CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0 || charSequence.toString().trim().isEmpty();
    }

    public static boolean isNotBlank(CharSequence charSequence) {
        return !isBlank(charSequence);
    }
}
