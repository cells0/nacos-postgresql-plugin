package com.cell.nacos.plugin.postgresql.common;

/**
 * @author 赵凯
 * @since 2023-10-18
 */
public interface PgConstants {

    String PG_PLATFORM = "postgresql";
}
