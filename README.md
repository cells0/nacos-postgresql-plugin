# Nacos PostgreSQL Plugin

## 说明

支持 Nacos 使用 PostgreSQL 数据库的插件。

## 使用方法

1. 将此插件和 PostgreSQL 的驱动 jar 包（**共两个 jar**）放入 ${nacos.home}/plugins 目录下（无目录需要手动创建）。
2. 创建 PostgreSQL 数据库，并执行 file/postgresql-schema.sql 文件的内容。
3. 修改配置如下：

![配置](./file/1.png)

4. 启动 Nacos。
